/**
 * Created by teisaacs on 6/27/14.
 */

var speakersModule = angular.module('dwGuide.Speakers', ['ngRoute', 'dwGuide.firebase', 'firebase', 'dwGuide.Auth']);

/**
 * Setup routes for this modulex
 */
speakersModule.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/speakers', {templateUrl: 'src/speakers/speakers.tpl.html', controller: 'SpeakersCtrl'} );
    $routeProvider.when('/speakers/:speakerID', {templateUrl: 'src/speakers/addEditSpeaker.tpl.html', controller: 'SpeakerDetailCtrl'});
    $routeProvider.when('/speakers/deleteSpeaker/:speakerID', {templateUrl: 'src/speakers/deleteConfirm.tpl.html', controller: 'DeleteConfirmCtrl'});

}]);

/**
 * Speakers Controller for managing a list of speakers
 */
speakersModule.controller('SpeakersCtrl', function($scope, Service, $location) {

    $scope.speakers = Service.getSpeakers();

    $scope.getSessionName = function(sessionId) {
        return Service.getSessionNameByID(sessionId);
    };

    $scope.addEditSpeaker = function(speakerID) {
        if (speakerID) {
            $location.path('/speakers/' + speakerID);
        } else {
            $location.path('/speakers/ADD');
        }
    };

    $scope.deleteSpeaker = function(speakerID) {
        if (speakerID) {
            $location.path('/speakers/deleteSpeaker/' + speakerID);
        }
    };
});

/**
 * Speaker detail controller for managing individual speakers
 */
speakersModule.controller('SpeakerDetailCtrl', function($scope, $rootScope, $routeParams, Service, $location){
    var speakerID =  $routeParams.speakerID;

    //grab the speaker from firebase or create a new speaker object
    if (speakerID === 'ADD') {
        $scope.speaker = {}
    } else {
        $scope.speaker = Service.getSpeaker(speakerID);
    }

    $scope.cancel = function() {
        $location.path('/speakers');
    };


    $scope.addEditSpeakerSubmit = function() {
        if ($scope.speaker.$id) {
            //update mode
            Service.updateSpeaker($scope.speaker).then(function() {
                //success
                $location.path('/speakers');
            }, function(error) {
                //error
                Service.handleError(error, '/speakers');
            });
        } else {
            //add mode
            Service.addSpeaker($scope.speaker).then(function(ref) {
                //success
                $location.path('/speakers');
            }, function(error) {
                //error
                Service.handleError(error, '/speakers');
            });
        }
    };

});

/**
 * Delete Controller
 */
speakersModule.controller('DeleteConfirmCtrl', function($scope, $rootScope, $location, $routeParams, Service){
    var speakerID =  $routeParams.speakerID;
    $scope.speaker = Service.getSpeaker(speakerID);

    $scope.deleteSpeaker = function() {
        Service.deleteSpeaker($scope.speaker.$id).then(function(ref) {
            //success
            $location.path('/speakers');
        }, function(error) {
            //error
            Service.handleError(error, '/speakers');
        });
    };

    $scope.cancelDelete = function() {
        $location.path('/speakers');
    };
});
