/**
 * Created by teisaacs on 7/7/14.
 */

angular.module('dwGuide.firebase', ['firebase', 'dwGuide.Auth'])

//NOTE: Firebase is not an angular resource so can't be injected
    .factory('Service', function(Firebase, $firebase, $rootScope, AuthService, $location, APP_CONSTANTS){
        var ref = new Firebase(APP_CONSTANTS.FIREBASE_URL);
        var angularFire = $firebase(ref);

        var speakers = angularFire.$child('speakers');
        var sessions = angularFire.$child('sessions');

        //SPEAKERS
        var getSpeakers = function() {
            return speakers;
        };

        var getSpeaker = function(id) {
            return speakers.$child(id);
        };

        var addSpeaker = function(speaker) {
            return speakers.$add(speaker);
        };

        var deleteSpeaker = function(id) {
            return speakers.$remove(id);
        };

        var updateSpeaker = function(speaker) {
            return  speaker.$save();
        };

        //SESSIONS
        var getSessions = function() {
            return sessions;
        };

        var getSessionNameByID = function(id) {
            return sessions.$child(id).name;
        }


        var getSession = function(id) {
            return sessions.$child(id);
        }


        var handleError = function(error, redirectPath, errMsg) {
            if (error.code === 'PERMISSION_DENIED') {

                if (!$rootScope.currentUser) {
                    //need to login
                    AuthService.sendToLogin(redirectPath, 'Login Required' );
                } else {
                    //user does not have permission
                    alert('Invalid Permissions');
                    $location.path(redirectPath);
                }
            }

        };

        //return object literal
        return {
            getSpeakers: getSpeakers,
            getSpeaker: getSpeaker,
            addSpeaker: addSpeaker,
            deleteSpeaker: deleteSpeaker,
            updateSpeaker: updateSpeaker,
            getSessions: getSessions,
            getSessionNameByID: getSessionNameByID,
            getSession: getSession,

            handleError: handleError
        };
    });