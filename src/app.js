/**
 * Application file defines the dependencies of the application and sets the default routes.
 *
 * Created by teisaacs on 6/26/14.
 */
var app = angular.module('dwGuide', [
    'ngRoute',
    'dwGuide.Auth',
    'dwGuide.Speakers',
    'dwGuide.Sessions',
    'dwGuide.firebase'
]);

app.constant('APP_CONSTANTS', {FIREBASE_URL: 'https://dwguide.firebaseio.com'});

/**
 * Setup the default route
 */
app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {templateUrl: 'src/home.tpl.html'});
    $routeProvider.otherwise({redirectTo: '/home'});
}]);


/**
 * Application level controller for main page
 */
app.controller('AppCtrl', function($rootScope, $scope, $location, AuthService){

    $scope.showSpeakers = function() {
        $location.path('/speakers');
    }

    $scope.showSessions = function() {
        $location.path('/sessions');
    }

    $scope.logout = function() {
        AuthService.logout();
    }

    $scope.login = function() {
        AuthService.sendToLogin($location.path());
    }
});

