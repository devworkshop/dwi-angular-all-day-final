/**
 * Created by teisaacs on 6/26/14.
 */

//Define the module to add this logic to
var authModule = angular.module('dwGuide.Auth', ['ngRoute']);

/**
 * Setup the routes for this module
 */
authModule.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/login', {templateUrl: 'src/auth/login.tpl.html', controller: 'LoginCtrl', controllerAs: 'ctrl'});
}]);


/**
 * Initialize the auth module
 */
authModule.run(function($rootScope, AuthService, $location, $firebaseSimpleLogin, APP_CONSTANTS) {
    //setup authentication

    var dataRef = new Firebase(APP_CONSTANTS.FIREBASE_URL);
    $rootScope.loginService = $firebaseSimpleLogin(dataRef);

    $rootScope.loginService.$getCurrentUser().then(
        function(user) {
            $rootScope.currentUser = user;
        }
    );

    $rootScope.$on("$firebaseSimpleLogin:logout", function(event) {
        console.log("successfully logged out!");
        //$location.path('home');
    });

    $rootScope.$on("$firebaseSimpleLogin:login", function(e, user) {
        console.log("User " + user.id + " successfully logged in!");
        //workaround for google redirect
        if (user.provider === 'google') {
            $rootScope.currentUser = user;
            $location.path("home");
        }
    });
});

/**
 * Authentication service
 */
authModule.service('AuthService', function($rootScope, $location) {

    var isAuthenticated = function () {
        return ($rootScope.currentUser) ? true : false;
    }

    //This message will be checked by the login controller
    var loginMessage = '';

    //default redirect path
    var redirectPath = '/home';

    var sendToLogin = function(redirectPath, loginMessage) {
        this.redirectPath = redirectPath;
        this.loginMessage = (loginMessage) ? loginMessage : 'Authorized Users Only' ;
        $location.path('/login');
    };

    var logout = function() {
        $rootScope.loginService.$logout();
        $rootScope.currentUser = null;

        $location.path('/home');
    }

    return {
        isAuthenticated: isAuthenticated,
        loginMessage: loginMessage,
        sendToLogin: sendToLogin,
        logout: logout

    };
});


/**
 * Login controller
 */
authModule.controller('LoginCtrl', function($scope, $rootScope, AuthService, $location) {

    //form variables
    //login error default to message on the model, this is set by other places in the app
    $scope.errorMsg = AuthService.loginMessage;
    $scope.username = '';
    $scope.password = '';


    //login function
    $scope.authenticate = function() {
        //reset the error message
        $scope.errorMsg = "";

        if ($scope.username && $scope.password) {
            if ($scope.registerUser) {
                $scope.registerEmail($scope.username, $scope.password, false);
            } else {
                //use the Firebase loginService to authenticate user
                $rootScope.loginService.$login('password', {
                    email: $scope.username,
                    password: $scope.password
                }).then(function(user) {
                    $rootScope.currentUser = user;
                    $location.path(AuthService.redirectPath);
                }, function(error) {
                    console.log('Login failed: ', error);
                    $scope.errorMsg = "Invalid Login Credentials"
                });
            }
        }
    }

    /**
     * Registers a users email with Firebase email authentication
     */
    $scope.registerEmail = function(email, password, noLogin) {
        $rootScope.loginService.$createUser(email, password, noLogin).then(function(user) {
            //SUCCESS - Created and logged in user, the login event is not hit for some reason
            $rootScope.currentUser = user;
            $location.path(AuthService.redirectPath);
        }, function(error) {
            //ERROR
            $scope.errorMsg = "Error creating user"
        });
    };


    $scope.authenticateGoogle = function() {
        $rootScope.loginService.$login('google', {
            preferRedirect: true,
            rememberMe: true
        }).then(function(user){
            // handled by login event
        }, function(error) {
            console.log('Login failed: ', error);
            $scope.errorMsg = "Invalid Login Credentials"
        });
    };

    $scope.authenticateTwitter = function() {
        $rootScope.loginService.$login('twitter').then(function(user) {
            console.log('Logged in as: ', user.uid);
            // handled by login event
        }, function(error) {
            console.log('Login failed: ', error);
            $scope.errorMsg = "Invalid Login Credentials"
        });
    };
});

