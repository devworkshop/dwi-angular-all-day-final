/**
 * Created by teisaacs on 6/27/14.
 */

var sessionsModule = angular.module('dwGuide.Sessions', ['ngRoute', 'dwGuide.firebase']);


speakersModule.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/sessions', {templateUrl: 'src/sessions/sessions.tpl.html', controller: 'SessionsCtrl'} );
    $routeProvider.when('/sessions/:sessionID', {templateUrl: 'src/sessions/addEditSession.tpl.html', controller: 'SessionDetailCtrl'});
}]);


speakersModule.controller('SessionsCtrl', function($scope, Service) {
    $scope.sessions = Service.getSessions();
});


speakersModule.controller('SessionDetailCtrl', function($scope, Service, $routeParams) {
    var sessionID =  $routeParams.sessionID;
    $scope.session = Service.getSession(sessionID);
});